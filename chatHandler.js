const User = require("./Models/user");

const { CommunicationIdentityClient } = require('@azure/communication-identity');

//creating the identity client
const connectionString = process.env['COMMUNICATION_SERVICES_CONNECTION_STRING'];

// Instantiate the identity client
const identityClient = new CommunicationIdentityClient(connectionString);

module.exports.createIdentity = async (user_id) => {
    let user = await User.findOne({ _id: user_id });
    if (!user.chatIdentity) {
        let identityResponse = await identityClient.createUser();

        console.log(`\nCreated an identity with ID: ${identityResponse.communicationUserId}`);

        user.chatIdentity = identityResponse;
        await user.save();
    }
}

module.exports.getToken = async (identityResponse) => {
    // Issue an access token with the "voip" scope for an identity
    let tokenResponse = await identityClient.getToken(identityResponse, ["chat"]);

    // Get the token and its expiration date from the response
    const { token, expiresOn } = tokenResponse;

    // Print the expiration date and token to the screen
    console.log(`\nIssued an access token with 'chat' scope that expires at ${expiresOn}:`);
    console.log(token);

    return tokenResponse;
}