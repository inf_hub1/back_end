const User = require("./Models/user.js");

module.exports.get_influencers = (req, res) => {
    
    if (!req.query.categories || req.query.categories == [ '' ]) {
        console.log(req.query.categories);
        User.find({ is_infl: true })
            .then((users) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(users);
                res.end();
            }, (err) => console.log(err))
            .catch((err) => console.log(err));
    } else {
        User.find({ is_infl: true, $and: req.query.categories.map(category => ({ "categories": category })) })
            .then((users) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(users);
                res.end();
            }, (err) => console.log(err))
            .catch((err) => { console.log(err); res.status(400).send("something went wrong") });
    }
}
module.exports.become_influencers = (req, res) => {
    let user_id = req.user.user_id;
    User.findByIdAndUpdate(user_id, {
        //$push: {categories: req.body.categories},
        categories: req.body.categories,
        youtube_url: req.body.youtub_url,
        instagram_url: req.body.instagram_url
    })
        .then((user) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.end();
        }, (err) => console.log(err))
        .catch((err) => console.log(err));



}