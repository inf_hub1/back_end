const express = require("express");
const router = express.Router();
const User = require("../Models/user");
const { ChatClient } = require('@azure/communication-chat');
const { AzureCommunicationTokenCredential } = require('@azure/communication-common');

const chatHandler = require("../chatHandler");
const auth = require("../middleware/auth");

router.get("/identity/create", auth, (req, res) => {
    if (req.user) {
        chatHandler.createIdentity(req.user.user_id);
    }
});


router.get("/identity/:user_id", auth, async (req, res) => {
    if (req.user) {
        const user = await User.findById(req.params.user_id || req.user.user_id);
        res.send(user.chatIdentity["communicationUserId"]);
    }
    //res.send("stuff")
})

router.get("/token", auth, async (req, res) => {
    let user = await User.findById(req.user.user_id);
    console.log(user);
    if (user.chatIdentity) {
        console.log("generating token...");
        const { token, expiresOn } = await chatHandler.getToken(user.chatIdentity);
        res.status(200).send(token);
    } else {
        console.log("user has no chat identity. creating one...");
        await chatHandler.createIdentity(req.user.user_id);
        console.log("generating token...");
        user = await User.findById(req.user.user_id);
        const { token, expiresOn } = await chatHandler.getToken(user.chatIdentity);
        res.status(200).send(token);
    }
});

const ChatThread = require("../Models/chatThread");

router.get("/thread/:user_id", auth, async (req, res) => {
    console.log(req.params.user_id);
    if (req.user && req.params.user_id) {
        const thread = await ChatThread.findOne({
            $or: [
                { userId1: req.params.user_id, userId2: req.user.user_id },
                { userId1: req.user.user_id, userId2: req.params.user_id }
            ]
        });
        res.status(200).json(thread);
    }
});

router.post("/thread/:user_id", auth, async (req, res) => {
    console.log("storing thread id...")
    if (req.user && req.params.user_id && typeof req.user !== "undefined" && typeof req.params.user_id !== "undefined") {
        ChatThread.create({
            threadId: req.body.threadId,
            userId1: req.user.user_id,
            userId2: req.params.user_id
        }).then(res.status(200).json({ success: true })).catch((error) => { res.status(400).send("an error occured"); console.log(error) });
    }
});

router.get("/contacts", auth, async (req, res) => {
    if (req.user) {
        let contacts = await ChatThread.find({
            $or: [
                { "userId1": req.user.user_id },
                { "userId2": req.user.user_id }
            ]
        });
        console.log("hhhhhhhhhhhhhhhhhhh",contacts);

        if (contacts) {
            contacts = await Promise.all(contacts.map(async (contact) => {
                let userId = (contact.userId1 == req.user.user_id) ? contact.userId2 : contact.userId1;
                let user = await User.findById(userId);
                if (user) {
                    return {
                        influence_id: user._id,
                        username: user.username,
                        picture_url: user.profile_picture,
                        number_followers: user.number_followers,
                        categories: user.categories,
                    }
                } else {
                    return null;
                }
            }))
            console.log(contacts)
        } else {
            contacts = [];
        }

        res.status(200).send(contacts);
    }
})

module.exports = router;