const express = require('express');
require("dotenv").config();
const auth_router = express.Router();
const User = require("../Models/user.js");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
let sessionLength = "1h";



auth_router
    .post("/register", async (req, res) => {
        try {
            const { username, email, password } = req.body;
            console.log(req.body);

            if (!(username && email && password)) {
                res.status(400).send("All inputs are required");
            }

            // TODO: better way to do this -----------------------
            var oldUser = await User.findOne({ email: email });

            if (oldUser) {
                return res.status(409).send("User with this email already Exists");
            }

            oldUser = await User.findOne({ username: username });
            if (oldUser) {
                return res.status(409).send("User with this username already Exists");
            }
            //----------------------------------------------------

            const hashedPassword = await bcrypt.hash(password, 10);

            const user = await User.create({
                username,
                email: email.toLowerCase(),
                password: hashedPassword,
            });

            const token = jwt.sign(
                {
                    user_id: user._id,
                    username: user.username,
                    email: user.email,
                    role: user.role
                },
                process.env.TOKEN_KEY, //TODO: how can this be more secure?
                {
                    expiresIn: sessionLength
                }
            );

            user.token = token;

            res.status(201).json({
                "user": user,
                "expiresIn": Number(sessionLength.slice(0, -1)) * 3600 * 1000,
            })
        } catch (err) {
            console.log(err);
            //TODO: response with error code
        }
    })
    .post("/login", async (req, res) => {
        try {
            const { email, password } = req.body;

            if (!(email && password)) {
                res.status(400).send("All inputs are required");
                return
            }

            const user = await User.findOne({ email });

            if (user && (await bcrypt.compare(password, user.password))) {
                const token = jwt.sign(
                    {
                        user_id: user._id,
                        username: user.username,
                        email: user.email,
                        role: user.role
                    },
                    process.env.TOKEN_KEY,
                    {
                        expiresIn: sessionLength,
                    }
                );
                user.token = token;
                res.status(200).json({
                    "user": user,
                    "expiresIn": Number(sessionLength.slice(0, -1)) * 3600 * 1000,
                });
            } else {
                console.log("Invalid Credentials");
                res.status(400).send("Invalid Credentials");
            }
        } catch (err) {
            console.log(err);
        }
    })

module.exports=auth_router