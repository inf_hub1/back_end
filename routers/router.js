const express = require('express');
const router = express.Router();

const auth = require("../middleware/auth");
const requestHandlers = require("../requestHandlers");
const User = require("../Models/user.js");

router.get("/influencers",(req,res)=>requestHandlers.get_influencers(req,res))
.post("/influencers",auth,(req,res)=>requestHandlers.become_influencers(req,res))
.get("/:user_id/username",(req,res)=>{

    let user_id=req.params.user_id;
    User.findOne({ _id: user_id })
            .then((user) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(user.username);
                res.end();
            }, (err) => console.log(err))
            .catch((err) => console.log(err));

})

module.exports = router;