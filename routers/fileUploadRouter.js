const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');

const User = require("../Models/user");

const auth = require("../middleware/auth");

const fs = require('fs')
const util = require('util')
const unlinkFile = util.promisify(fs.unlink)

const upload = multer({ dest: 'files/' })

const { uploadFile, getFileStream } = require('../s3.js')


router.post('/', auth, upload.single('avatar'), async (req, res) => {

  let user_id = req.user.user_id;
  const file = req.file
  if (user_id) file.filename = user_id +"."+ file.originalname.split('.')[1];

  const result = await uploadFile(file)
  await unlinkFile(file.path);

  const description = req.body.description;
  User.findByIdAndUpdate(user_id, {
    profile_picture: result.Location,
    is_infl: true
  })
    .then((user) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.end();
    }, (err) => console.log(err))
    .catch((err) => console.log(err));
})

module.exports = router;