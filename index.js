const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const cors = require('cors');
const path = require('path');

const auth_router=require('./routers/authRouter.js');
const router = require("./routers/router");
const chatRouter = require("./routers/chatRouter");
const fileUploadRouter = require("./routers/fileUploadRouter");

const bodyparser = require('body-parser');
app.use(bodyparser.json({ limit: '50mb' }));
app.use(bodyparser.urlencoded({ limit: '50mb', extended: true }));

app.use(express.static(path.join(__dirname + "/uploads")));

app.use(cors());
app.use('/',auth_router);
app.use('/', router);
app.use('/chat', chatRouter);
app.use('/file', fileUploadRouter);


server.listen(4000, () => {
    console.log('listening on *:4000');
}); 
//data base
const mongoose = require('mongoose');
const url = 'mongodb://127.0.0.1:27017/ADfluencer';
const connect = mongoose.connect(url);
connect.then((db) => {
    console.log("Connected correctly to the data base server");
}, (err) => { console.log(err); });