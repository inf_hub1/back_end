const mongoose =require('mongoose');
const Schema =mongoose.Schema;

const threadSchema = new Schema({
    threadId: {type: String, required:true},
    userId1: {type: String, required:true},
    userId2: {type: String, required:true},
},{timestamps :true});

var threadSession=mongoose.model('threadSession', threadSchema);

module.exports= threadSession;