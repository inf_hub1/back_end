const mongoose =require('mongoose');
const Schema =mongoose.Schema;

const userSchema = new Schema({
    is_infl:Boolean,
    profile_picture:String,
    number_followers:String,
    categories:Array,
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    token: { type: String },
    chatIdentity: {type:Object},
    instagram_url:String,
    youtube_url:String,
},{timestamps :true});

var user=mongoose.model('user',userSchema);

module.exports=user;
